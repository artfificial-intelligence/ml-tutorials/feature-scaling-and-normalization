# 피처 스케일링과 정규화

## <a name="intro"></a> 개요
이 포스팅에서는 기계 학습 모델의 데이터 전처리에서 필수적 단계인 피처 스케일링과 정규화 기법에 대해 설명할 것이다.

- 기능 확장 및 정규화가 무엇이며 왜 중요한가
- 다양한 타입의 피처 스케일링과 정규화 기법, 그리고 그 작동 방식
- scikit-learn 라이브러리를 이용하여 Python에서 특징 스케일링과 정규화 기법 적용 방법
- 샘플 데이터세트에 대한 다양한 기법의 효과 비교

이 긍의 읽기를 마치고 이해한다면 피처를 효과적으로 스템링하고 정규화할 수 있으며 기계 학습 모델의 성능을 향상시킬 수 있다

## <a name="sec_02"></a> 피처 스케일링과 정규화가 중요한 이유
피처 스케일링과 정규화는 기계 학습 모델의 성능과 정확도에 영향을 미칠 수 있기 때문에 중요하다. 이 절에서는 다음을 설명한다.

- 피처는 무엇이며 스테일링과 정규화가 필요한 이유
- 스케일링되지 않은 피처와 정규화되지 않은 피처에서 발생하는 일반적인 문제
- 기계 학습 모델에 대한 스케일링과 정규화의 이점

### 피처는 무엇이며 스테일링과 정규화가 필요한 이유
피처는 데이터를 설명하는 변수나 속성이며, 기계 학습 모델의 입력으로 사용된다. 예를 들어, 주택 가격을 예측하기 위해 모델을 구축하는 경우, 피처 중 일부는 집의 크기, 침실 수, 위치, 연수 등이 될 수 있다.

피처 스케일링과 정규화는 각각 피처를 공통의 스케일과 분포로 변환하는 과정이다. 스케일링은 피처의 범위 또는 크기를 조정하는 것을 의미하는 반면, 정규화는 특징의 모양 또는 퍼짐을 조정하는 것을 의미한다.

상이한 피처들이 상이한 스케일과 분포를 가질 수 있고, 이는 기계 학습 모델들에 대한 문제를 야기할 수 있기 때문에 피처 스케일링과 정규화가 필요하다. 예를 들어, 일부 피처들은 매우 크거나 매우 작은 값을 가질 수 있고, 다른 피처들은 0에 가까운 값을 가질 수 있다. 일부 피처들은 정규 분포 또는 가우시안 분포를 가질 수 있고, 다른 피처들은 치우치거나 지수 분포를 가질 수 있다.

### 스케일링되지 않은 피처와 정규화되지 않은 피처에서 발생하는 일반적인 문제
스케일링되지 않은 피처와 정규화되지 않은 피처에서 발생하는 일반적인 문제는 다음과 같다:

- **경사 하강 수렴(gradient descent convergence)**: 많은 기계 학습 모델은 최적의 매개 변수를 찾기 위한 최적화 알고리즘으로 경사 하강을 사용한다. 경사 하강은 비용 함수의 가장 가파른 하강 방향으로 매개 변수를 업데이트함으로써 작동한다. 그러나 피처가 다른 척도를 가질 경우 비용 함수는 길쭉한 모양을 가질 것이고, 이는 경사 하강이 수렴하는 데 더 오래 걸리거나 심지어 로컬 최소값에 갇히게 될 수 있다. 피처의 스케일링은 비용 함수를 더 구형이고 매끄럽게 만들 수 있으며, 이는 수렴 속도를 높이고 모델의 정확도를 향상시킬 수 있다.
- **거리 기반 알고리즘(distance-based algorithm)**: 일부 기계 학습 모델은 거리 기반 알고리즘을 사용하여 데이터 포인트 간의 유사성 또는 비유사성을 측정한다. 예를 들어, k-최근접 이웃(KNN)과 k-평균 클러스터링은 유클리드 거리를 사용하여 가장 가까운 이웃 또는 클러스터 중심을 찾는다. 그러나 피처의 척도가 다를 경우, 거리가 더 큰 값을 가진 피처에 의해 지배되어 결과가 왜곡되고 모델의 성능이 저하될 수 있다. 피처의 척도를 조정하면 거리를 더 균형 있고 의미 있게 만들 수 있다.
- **정규화(Regularization)**: 일부 기계 학습 모델은 과적합을 방지하고 일반화를 개선하기 위해 정규화를 사용한다. 정규화는 매개 변수의 크기에 따라 달라지는 비용 함수에 페널티 항을 추가함으로써 작동한다. 그러나 피처의 척도가 다른 경우 정규화는 값이 작은 피처의 매개 변수보다 값이 큰 피처의 매개 변수에 더 많은 페널티를 주어 모델에 편향을 초래하고 정확도를 저하시킬 수 있다. 피처의 척도를 조정하면 정규화를 보다 공정하고 효과적으로 만들 수 있다.
- **이상치 및 잡음(Outliers and noise)** 일부 기계 학습 모델은 나머지 데이터에서 크게 벗어나는 데이터 포인트인 이상치와 잡음에 민감하다. 이상치와 잡음은 피처의 평균과 표준 편차에 영향을 미쳐 분포를 왜곡시키고 데이터의 품질을 저하시킬 수 있다. 피처를 정규화하면 이상치와 잡음의 영향을 줄이고 분포를 더 안정적이고 강건하게 만들 수 있다.

### 기계 학습 모델에 대한 스케일링과 정규화의 이점
기계 학습 모델에 대한 스케일링과 정규화의 이점은 다음과 같다.

- **성능과 정확도 향상**: 스케일링과 정규화는 최적화, 거리 측정, 정규화 및 데이터 품질을 보다 신뢰할 수 있고 일관되게 만들어 기계 학습 모델의 성능과 정확도를 향상시킬 수 있다.
- **계산 비용과 시간 감소**: 스케일링과 정규화는 경사 하강이 더 빠르게 수렴되고 거리 계산이 더 간단해지고 정규화가 덜 복잡해지고 데이터에 잡음이 덜 발생함으로써 기계 학습 모델의 계산 비용과 시간을 줄일 수 있다.
- **해석과 비교 가능성 향상**: 스케일링과 정규화는 피처가 공통 척도와 분포를 갖도록 하여 기계 학습 모델의 해석 가능성과 비교 가능성을 높일 수 있으며, 이는 결과의 분석과 평가를 용이하게 할 수 있다.

다음 절에서는 다양한 타입의 피처 스케일링과 정규화 기법과 그 작동 방식에 대해 알아본다.

## <a name="sec_03"></a> 피처 스케일링과 정규화 기법의 종류
이 절에서는 다양한 타입의 피처 스케일링과 정규화 기법, 그리고 그 작동 방식에 대해 설명한다.

- 일반적인 피처 스케일링과 정규화 기법 및 그 공식
- 각 기법의 장점과 단점
- 데이터와 문제에 적합한 기법 선택

### 일반적인 피처 스케일링과 정규화 기법 및 그 공식
많은 피처 스케일링과 정규화 기법들이 있지만, 가장 일반적인 것들은 다음과 같다.

- **Min-max 스케일링**: 이 기법은 피처 값에서 피처들의 최소값을 빼고 피처의 범위로 나눔으로써 피처를 고정된 범위(보통 [0, 1])로 스케일링한다. 그 공식은 다음과 같다.

```python
# x is the original feature value
# x_min is the minimum value of the feature
# x_max is the maximum value of the feature
# x_scaled is the scaled feature value
x_scaled = (x - x_min) / (x_max - x_min)
```

- **표준화**: 이 기법은 피처 값에서 피처의 평균을 빼고 피처의 표준 편차로 나눔으로써 특징이 0 평균과 단위 분산을 갖도록 스케일링한다. 그 공식은 다음과 같다.

```python
# x is the original feature value
# x_mean is the mean of the feature
# x_std is the standard deviation of the feature
# x_scaled is the scaled feature value
x_scaled = (x - x_mean) / x_std
```

- **강건한(robust) 스케일링**: 이 기법은 피처 값에서 피처의 중앙값을 빼고 피쳐의 사분위간 범위로 나눔으로써 피쳐를 0 중앙값과 단위 사분위간 범위로 스케일링한다. 사분위간 범위는 피쳐의 75번째 백분위수와 25번째 백분위수 사이의 차이이다. 그 공식은 다음과 같다.

```python
# x is the original feature value
# x_med is the median of the feature
# x_iqr is the interquartile range of the feature
# x_scaled is the scaled feature value
x_scaled = (x - x_med) / x_iqr
```

- **정규화**: 이 기법은 피처 벡터를 그 노름으로 나눔으로써 피처가 단위 노름을 갖도록 스케일링한다. 노름은 유클리드 노름, 맨해튼 노름 또는 맥스 노름과 같은 다른 방법을 사용하여 계산할 수 있는 벡터의 길이 또는 크기이다. 그 공식은 다음과 같다.

```python
# x is the original feature vector
# x_norm is the norm of the feature vector
# x_scaled is the scaled feature vector
x_scaled = x / x_norm
```

### 각 기법의 장점과 단점
각 기법은 데이터와 문제에 따라 각각의 장단점이 있다. 각 기법의 장단점을 소개하면 다음과 같다.

#### Min-max 스케일링

**Pros**:

- 피처의 원래 분포와 모양을 보존한다.
- 구현과 해석이 용이하다.
- 신경망과 k-means 클러스터링 같이 고정된 범위를 필요로 하는 알고리즘에 유용하다.

**Cons**:

- 이상치와 노이즈에 민감하여 최소값과 최대값에 영향을 미치고 스케일링을 왜곡시킬 수 있다.
- 음의 값을 양의 범위로 이동시키기 때문에 음의 값을 잘 처리하지 못한다.
- 거리 계산과 정규화를 지배할 수 있는 큰 값의 중요성을 감소시키지 않는다.

#### 표준화

**Pros**:

- 피처를 0을 중심으로 하고 단위 분산을 갖도록 스케일링한다.
- 극단값의 영향을 덜 받는 평균과 표준편차를 사용하기 때문에 이상치와 노이즈에 강하다.
- 선형 회귀와 로지스틱 회귀와 같이 정규 분포를 가정하는 알고리즘에 유용하다.

**Cons**:

- 왜도(skewness)와 첨도(kurtosis)를 변화시킬 수 있어 본래의 분포와 형상을 보존하지 못한다.
- 매우 크거나 매우 작은 값을 생성할 수 있기 때문에 고정된 범위를 갖지 않는다.
- 피처에 따라 평균과 표준 편차가 다를 수 있기 때문에 공통 척도를 보장하지 않는다.

#### 강건한 스케일링

**Pros**:

- 피처를 0 주변에 중심을 두고 단위 사분위간 범위를 갖도록 스케일링한다.
- 극단치의 영향을 받지 않는 중위수와 사분위간 범위를 사용하기 때문에 이상치와 노이즈에 매우 강건하다.
- 라소(kasso) 또는 능선(ridge) 정규화를 갖는 서포트 벡터 머신과 선형 모델 같이 이상치에 민감한 알고리즘에 유용하다.

**Cons**:

- 왜도와 첨도를 변화시킬 수 있어 본래의 분포와 형상을 보존하지 못한다.
- 매우 크거나 매우 작은 값을 생성할 수 있기 때문에 고정된 범위를 갖지 않는다.
- 피처에 따라 중앙값과 사분위간 범위가 다를 수 있기 때문에 공통 척도를 보장하지 않는다.

#### 정규화

**Pros**:

- 이는 피처가 동일한 길이 또는 크기를 갖는 단위 노름을 갖도록 확장한다.
- 표준에만 의존하기 때문에 척도와 피처의 분포에 불변한다.
- k-최근접 이웃, k-평균 클러스터링과 같이 벡터 간의 각도 또는 코사인 유사도를 사용하는 알고리즘에 유용하다.

**Cons**:

- 벡터 사이의 상대적인 거리와 각도를 변경할 수 있기 때문에 피처의 원래 분포와 모양을 보존하지 않는다.
- 매우 크거나 매우 작은 값을 생성할 수 있기 때문에 고정된 범위를 갖지 않는다.
- 거리 계산과 정규화를 지배할 수 있는 큰 값의 중요성을 감소시키지 않는다.

### 데이터와 문제에 적합한 기법 선택
이 질문에 대한 확실한 답은 없다. 서로 다른 기술이 서로 다른 데이터와 문제에 대해 더 잘 작동할 수도 있고 더 잘 작동하지 않을 수도 있기 때문이다. 그러나 데이터와 문제에 적합한 기술을 선택하는 데 도움이 될 수 있는 일반적인 지침은 다음과 같다.

- 먼저 데이터를 탐색하고 척도, 분포, 이상치 및 피처의 노이즈와 같은 데이터의 특성을 이해해야 한다.
- 둘째, 피처의 범위, 분포, 거리 및 정규화와 같은 사용 중인 기계 학습 모델의 요구 사항과 가정을 고려해야 한다.
- 셋째, 다양한 기법을 사용하여 실험하고 모델의 성능, 정확성, 수렴성, 해석 가능성 등의 결과 비교해야 한다.
- 마지막으로 분석과 평가에 따라 데이터와 문제에 가장 적합한 기법을 선택해야 한다.

다음 절에서는 scikit-learn 라이브러리를 사용하여 Python에서 특징 스케일링와  정규화 기법을 적용하는 방법을 설명한다.

## <a name="sec_04"></a> Python에서 피처 스케일링 정규화를 적용하는 방법
이 절에서는 scikit-learn 라이브러리를 사용하여 Python에서 피처 스케일링과 정규화 기법을 적용하는 방법을 설명한다.

- 필요한 모듈과 라이브러리를 임포트하는 방법
- 샘플 데이터세트를 로드하고 탐색하는 방법
- scikit-learn 클래스와과 방법을 활용하여 다양한 기법을 적용하는 방법
- 도표와 통계량을 사용하여 서로 다른 기법의 결과를 비교하는 방법

### 필요한 모듈과 라이브러리를 임포트하는 방법
Python에서 피처 스케일링과 정규화 기법을 적용하려면 다음과 같은 모듈과 라이브러리를 임포트해야 한다.

- **numpy**: 이는 다차원 배열과 행렬, 그리고 이들에 대한 수학적 기능과 연산을 지원하는 라이브러리이다.
- **panda**: 데이터 프레임, 시리즈 등 데이터 분석과 조작을 위한 데이터 구조와 도구를 제공하는 라이브러리이다.
- **matplotlib**: 히스토그램, 산점도, 박스도 등의 플롯팅과 시각화 도구를 제공하는 라이브러리이다.
- **saeborn**: 이는 matplotlib에 대한 높은 수준의 인터페이스와 테마, 통계 및 범주형 플롯을 제공하는 라이브러리이다.
- **scipy**: 통계 테스트와 함수 같은 과학적이고 기술적인 컴퓨팅 도구를 제공하는 라이브러리이다.
- **sklearn**: 데이터 전처리, 모델 선정, 평가 등 기계 학습 도구를 제공하는 라이브러리이다.

다음 코드를 사용하여 이러한 모듈과 라이브러리를 가져올 수 있다.

```python
# Import the necessary modules and libraries
import numpy as np
import pandas as pd
import matplotlib as mpl
from matplotlib import cm
import matplotlib.pyplot as plt
from scipy import stats
from sklearn.datasets import fetch_california_housing
from sklearn.preprocessing import (
    MinMaxScaler,
    Normalizer,
    RobustScaler,
    StandardScaler,
    minmax_scale,
)

```

### 샘플 데이터세트를 로드하고 탐색하는 방법
피처 스케일링과 정규화 기법을 시연하기 위해, 우리는 [California Housing Dataset](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.fetch_california_housing.html)이라고 불리는 sikit-learn 라이브러리의 샘플 데이터세트을 사용할 것이다. 이 데이터세트에는 방 수, 범죄율, 세율, 중앙값 등 보스턴에 있는 약 506채의 주택에 대한 정보가 포함되어 있다. 이 데이터세트은 다음 코드를 사용하여 로드하고 탐색할 수 있다.

```python
# Load the California Housing Dataset
dataset = fetch_california_housing()
print(dataset.DESCR)    # Print the description of the dataset

# Convert the dataset to a pandas data frame
print(dataset.feature_names[0:6])
print(dataset.data.shape, dataset.target.shape)
df = pd.DataFrame(dataset.data, columns=dataset.feature_names)
# print(df.head())
print(df.describe())
print(df.info())
df_target = pd.DataFrame(dataset.frame, columns=["MedHouseVal"])
# print(df_target.head())
print(df_target.describe())
print(df_target.info())
```

다음과 같은 출력을 얻을 수 있다.

```
.. _california_housing_dataset:

California Housing dataset
--------------------------

**Data Set Characteristics:**

:Number of Instances: 20640

:Number of Attributes: 8 numeric, predictive attributes and the target

:Attribute Information:
    - MedInc        median income in block group
    - HouseAge      median house age in block group
    - AveRooms      average number of rooms per household
    - AveBedrms     average number of bedrooms per household
    - Population    block group population
    - AveOccup      average number of household members
    - Latitude      block group latitude
    - Longitude     block group longitude

:Missing Attribute Values: None

This dataset was obtained from the StatLib repository.
https://www.dcc.fc.up.pt/~ltorgo/Regression/cal_housing.html

The target variable is the median house value for California districts,
expressed in hundreds of thousands of dollars ($100,000).

This dataset was derived from the 1990 U.S. census, using one row per census
block group. A block group is the smallest geographical unit for which the U.S.
Census Bureau publishes sample data (a block group typically has a population
of 600 to 3,000 people).

A household is a group of people residing within a home. Since the average
number of rooms and bedrooms in this dataset are provided per household, these
columns may take surprisingly large values for block groups with few households
and many empty houses, such as vacation resorts.

It can be downloaded/loaded using the
:func:`sklearn.datasets.fetch_california_housing` function.

.. topic:: References

    - Pace, R. Kelley and Ronald Barry, Sparse Spatial Autoregressions,
      Statistics and Probability Letters, 33 (1997) 291-297

['MedInc', 'HouseAge', 'AveRooms', 'AveBedrms', 'Population', 'AveOccup']
(20640, 8) (20640,)
             MedInc      HouseAge      AveRooms     AveBedrms    Population      AveOccup      Latitude     Longitude
count  20640.000000  20640.000000  20640.000000  20640.000000  20640.000000  20640.000000  20640.000000  20640.000000
mean       3.870671     28.639486      5.429000      1.096675   1425.476744      3.070655     35.631861   -119.569704
std        1.899822     12.585558      2.474173      0.473911   1132.462122     10.386050      2.135952      2.003532
min        0.499900      1.000000      0.846154      0.333333      3.000000      0.692308     32.540000   -124.350000
25%        2.563400     18.000000      4.440716      1.006079    787.000000      2.429741     33.930000   -121.800000
50%        3.534800     29.000000      5.229129      1.048780   1166.000000      2.818116     34.260000   -118.490000
75%        4.743250     37.000000      6.052381      1.099526   1725.000000      3.282261     37.710000   -118.010000
max       15.000100     52.000000    141.909091     34.066667  35682.000000   1243.333333     41.950000   -114.310000
<class 'pandas.core.frame.DataFrame'>
RangeIndex: 20640 entries, 0 to 20639
Data columns (total 8 columns):
 #   Column      Non-Null Count  Dtype  
---  ------      --------------  -----  
 0   MedInc      20640 non-null  float64
 1   HouseAge    20640 non-null  float64
 2   AveRooms    20640 non-null  float64
 3   AveBedrms   20640 non-null  float64
 4   Population  20640 non-null  float64
 5   AveOccup    20640 non-null  float64
 6   Latitude    20640 non-null  float64
 7   Longitude   20640 non-null  float64
dtypes: float64(8)
memory usage: 1.3 MB
None
       MedHouseVal
count            0
unique           0
top            NaN
freq           NaN
<class 'pandas.core.frame.DataFrame'>
RangeIndex: 0 entries
Data columns (total 1 columns):
 #   Column       Non-Null Count  Dtype 
---  ------       --------------  ----- 
 0   MedHouseVal  0 non-null      object
dtypes: object(1)
memory usage: 132.0+ bytes
None
```

## <a name="sec_05"></a> 샘플 데이터세트에서의 다양한 기법 비교
이 절에서는 샘플 데이터세트에 대한 다양한 피처 스케일링과 정규화 기법의 결과를 비교할 것이다

- 기술을 적용하기 전과 적용한 후의 파처
- 기술이 통계와 피처 분포에 미친 영향
- 단순 선형 회귀 모형의 성능과 정확도에 미친 영향

### 기술을 적용하기 전과 적용한 후의 파처
기법을 적용하기 전과 적용한 후에 피차가 어떻게 보이는지 알아보기 위해 mathplot과 seaborn을 사용하여 원래 피처와 스테일된된 피처의 히스토그램을 그릴 수 있다. 이를 위해 다음 코드를 사용한다.

```python
# Author:  Raghav RV <rvraghav93@gmail.com>
#          Guillaume Lemaitre <g.lemaitre58@gmail.com>
#          Thomas Unterthiner
# License: BSD 3 clause

# Load the California Housing Dataset
dataset = fetch_california_housing()
print(dataset.DESCR)    # Print the description of the dataset

# Convert the dataset to a pandas data frame
print(dataset.feature_names[0:6])
print(dataset.data.shape, dataset.target.shape)
df = pd.DataFrame(dataset.data, columns=dataset.feature_names)
# df = housing.data
# print(df.head())
print(df.describe())
print(df.info())
df_target = pd.DataFrame(dataset.frame, columns=["MedHouseVal"])
# print(df_target.head())
print(df_target.describe())
print(df_target.info())

X_full, y_full = dataset.data, dataset.target
feature_names = dataset.feature_names

feature_mapping = {
    "MedInc": "Median income in block",
    "HouseAge": "Median house age in block",
    "AveRooms": "Average number of rooms",
    "AveBedrms": "Average number of bedrooms",
    "Population": "Block population",
    "AveOccup": "Average house occupancy",
    "Latitude": "House block latitude",
    "Longitude": "House block longitude",
}

# Take only 2 features to make visualization easier
# Feature MedInc has a long tail distribution.
# Feature AveOccup has a few but very large outliers.
features = ["MedInc", "AveOccup"]
features_idx = [feature_names.index(feature) for feature in features]
X = X_full[:, features_idx]

distributions = [
    ("Unscaled data", X),
    ("Data after standard scaling", StandardScaler().fit_transform(X)),
    ("Data after min-max scaling", MinMaxScaler().fit_transform(X)),
    # ("Data after max-abs scaling", MaxAbsScaler().fit_transform(X)),
    (
        "Data after robust scaling",
        RobustScaler(quantile_range=(25, 75)).fit_transform(X),
    ),
    ("Data after sample-wise L2 normalizing", Normalizer().fit_transform(X)),
]

# scale the output between 0 and 1 for the colorbar
y = minmax_scale(y_full)

# plasma does not exist in matplotlib < 1.5
cmap = getattr(cm, "plasma_r", cm.hot_r)

def create_axes(title, figsize=(16, 6)):
    fig = plt.figure(figsize=figsize)
    fig.suptitle(title)

    # define the axis for the first plot
    left, width = 0.1, 0.22
    bottom, height = 0.1, 0.7
    bottom_h = height + 0.15
    left_h = left + width + 0.02

    rect_scatter = [left, bottom, width, height]
    rect_histx = [left, bottom_h, width, 0.1]
    rect_histy = [left_h, bottom, 0.05, height]

    ax_scatter = plt.axes(rect_scatter)
    ax_histx = plt.axes(rect_histx)
    ax_histy = plt.axes(rect_histy)

    # define the axis for the zoomed-in plot
    left = width + left + 0.2
    left_h = left + width + 0.02

    rect_scatter = [left, bottom, width, height]
    rect_histx = [left, bottom_h, width, 0.1]
    rect_histy = [left_h, bottom, 0.05, height]

    ax_scatter_zoom = plt.axes(rect_scatter)
    ax_histx_zoom = plt.axes(rect_histx)
    ax_histy_zoom = plt.axes(rect_histy)

    # define the axis for the colorbar
    left, width = width + left + 0.13, 0.01

    rect_colorbar = [left, bottom, width, height]
    ax_colorbar = plt.axes(rect_colorbar)

    return (
        (ax_scatter, ax_histy, ax_histx),
        (ax_scatter_zoom, ax_histy_zoom, ax_histx_zoom),
        ax_colorbar,
    )


def plot_distribution(axes, X, y, hist_nbins=50, title="", x0_label="", x1_label=""):
    ax, hist_X1, hist_X0 = axes

    ax.set_title(title)
    ax.set_xlabel(x0_label)
    ax.set_ylabel(x1_label)

    # The scatter plot
    colors = cmap(y)
    ax.scatter(X[:, 0], X[:, 1], alpha=0.5, marker="o", s=5, lw=0, c=colors)

    # Removing the top and the right spine for aesthetics
    # make nice axis layout
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    ax.spines["left"].set_position(("outward", 10))
    ax.spines["bottom"].set_position(("outward", 10))

    # Histogram for axis X1 (feature 5)
    hist_X1.set_ylim(ax.get_ylim())
    hist_X1.hist(
        X[:, 1], bins=hist_nbins, orientation="horizontal", color="grey", ec="grey"
    )
    hist_X1.axis("off")

    # Histogram for axis X0 (feature 0)
    hist_X0.set_xlim(ax.get_xlim())
    hist_X0.hist(
        X[:, 0], bins=hist_nbins, orientation="vertical", color="grey", ec="grey"
    )
    hist_X0.axis("off")

def make_plot(item_idx):
    title, X = distributions[item_idx]
    ax_zoom_out, ax_zoom_in, ax_colorbar = create_axes(title)
    axarr = (ax_zoom_out, ax_zoom_in)
    plot_distribution(
        axarr[0],
        X,
        y,
        hist_nbins=200,
        x0_label=feature_mapping[features[0]],
        x1_label=feature_mapping[features[1]],
        title="Full data",
    )

    # zoom-in
    zoom_in_percentile_range = (0, 99)
    cutoffs_X0 = np.percentile(X[:, 0], zoom_in_percentile_range)
    cutoffs_X1 = np.percentile(X[:, 1], zoom_in_percentile_range)

    non_outliers_mask = np.all(X > [cutoffs_X0[0], cutoffs_X1[0]], axis=1) & np.all(
        X < [cutoffs_X0[1], cutoffs_X1[1]], axis=1
    )
    plot_distribution(
        axarr[1],
        X[non_outliers_mask],
        y[non_outliers_mask],
        hist_nbins=50,
        x0_label=feature_mapping[features[0]],
        x1_label=feature_mapping[features[1]],
        title="Zoom-in",
    )

    norm = mpl.colors.Normalize(y_full.min(), y_full.max())
    mpl.colorbar.ColorbarBase(
        ax_colorbar,
        cmap=cmap,
        norm=norm,
        orientation="vertical",
        label="Color mapping for values of y",
    )

# The output is as follows
# Original data
make_plot(0)
plt.show()

# StandardScaler
make_plot(1)
plt.show()

# MinMaxScaler
make_plot(2)
plt.show()

# RobustScaler
make_plot(3)
plt.show()

# Normalizer
make_plot(4)
plt.show()

```

이렇게 하면 다음과 같은 출력이 생성된다.

![](./images/original.png)

![](./images/standard.png)

![](./images/min-max.png)

![](./images/robust.png)

![](./images/normaliization.png)


그림에서 볼 수 있듯이, 기법을 적용하기 전에 피처들은 스케일과 분포가 서로 다르다. 기법을 적용한 후에는 기법에 따라 피처들의 범위와 모양이 다르다. 예를 들어, min-max 스케일링은 피처를 [0, 1] 범위로 이동시키고, 표준화는 피처를 0 주변으로 중심을 두고 단위 분산을 갖도록 스케일링하고, 강건 스케일링은 피처를 0 주변으로 중심을 두고 단위 사분위간 범위를 갖도록 스케일링하고, 정규화는 피처를 단위 규범을 갖도록 스케일링한다.

## <a name="summary"></a> 마치며
이 포스팅에서는 기계 학습 모델의 데이터 전처리에서 필수적인 단계인 피처 스케일링과 정규화 기법에 대해 설명하였다.

- 피처 스케일링과 정규화가 무엇이며 왜 중요한가
- 다양한 타입의 피처 스케일링과 정규화 기법과 그 작동 방식
- scikit-learn 라이브러리를 이용하여 Python에서 피처 스케일링과 정규화 기법 적용
- 도표와 통계량을 사용하여 서로 다른 기법의 결과를 비교
- 데이터와 문제에 적합한 기술을 선택

이 포스팅에 대하여 학습이 끝나면 피처를 효과적으로 스케일링하고 정규화할 수 있으며 기계 학습 모델의 성능과 정확도를 향상시킬 수 있다.

> **code test 완료**
