# 피처 스케일링과 정규화 <sup>[1](#footnote_1)</sup>

> <font size="3">기계 학습 모델의 피처를 확장하고 정규화하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./feature-scaling-and-normalization.md#intro)
1. [피처 스케일링과 정규화가 중요한 이유](./feature-scaling-and-normalization.md#sec_02)
1. [피처 스케일링과 정규화 기법의 종류](./feature-scaling-and-normalization.md#sec_03)
1. [Python에서 피처 스케일링 정규화를 적용하는 방법](./feature-scaling-and-normalization.md#sec_04)
1. [샘플 데이터세트에서의 다양한 기법 비교](./feature-scaling-and-normalization.md#sec_05)
1. [마치며](./feature-scaling-and-normalization.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 14 — Feature Scaling and Normalization Techniques](https://ai.plainenglish.io/ml-tutorial-14-feature-scaling-and-normalization-techniques-36d5a219890f?sk=7e947ed9c9fb567e6102e46d18d6f0b7)를 편역하였습니다.
